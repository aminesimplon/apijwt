<?php

namespace App\EventSubscriber;

use App\Entity\Story;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Symfony\EventListener\EventPriorities;
use Symfony\Component\HttpFoundation\Request;

class EanSubscriber implements EventSubscriberInterface
{
    public function onKernelView(ViewEvent $event): void
    {
        $story = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$story instanceof Story  || Request::METHOD_POST !== $method) {
            return;
        }

        if (!$story->getEan()) {
            $story->setEan(mt_rand(10000000, 99999999));
        }    
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onKernelView', EventPriorities::PRE_WRITE],
        ];
    }
}

